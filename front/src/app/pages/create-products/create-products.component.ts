import { Component, OnInit, ViewChild } from '@angular/core';
import { fetchCategoryRequest } from '../../store/category.actions';
import { Observable, Subscription } from 'rxjs';
import { CategoryModel } from '../../models/category.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/type';
import { NgForm } from '@angular/forms';
import { ProductData } from '../../models/product.moedl';
import { createProductRequest } from '../../store/products.actions';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-create-products',
  templateUrl: './create-products.component.html',
  styleUrls: ['./create-products.component.sass']
})
export class CreateProductsComponent implements OnInit{
  @ViewChild('form') form!: NgForm;
  categories: Observable<CategoryModel[]>;
  loading: Observable<boolean>;
  error: Observable<string | null>;
  user: Observable<null | User>;
  userSubscription!: Subscription;
  userToken!: string;

  constructor(private store: Store<AppState>) {
    this.categories = store.select(state => state.category.category);
    this.loading = store.select(state => state.products.createLoading);
    this.error = store.select(state => state.products.createError);
    this.user = store.select(state => state.user.user);
  }

  ngOnInit() {
    this.userSubscription = this.user.subscribe( user => {
      if (user) {
        this.userToken = user?.token
      }
    })
  }

  onSubmit() {
    const productData: ProductData = this.form.value;
    productData.token = this.userToken;
    this.store.dispatch(createProductRequest({productData}));
  }
}
