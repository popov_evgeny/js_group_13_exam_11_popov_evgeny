import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../../models/product.moedl';
import { environment } from '../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/type';
import { fetchProductCategoryRequest } from '../../store/products.actions';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.sass']
})
export class ProductCategoryComponent implements OnInit {
  products: Observable<Product[]>
  loading: Observable<boolean>
  error: Observable<null | string>
  apiUrl = environment.apiUrl;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
    ) {
    this.products = store.select(state => state.products.products);
    this.loading = store.select(state => state.products.fetchLoading);
    this.error = store.select(state => state.products.fetchError);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(fetchProductCategoryRequest({id: params['id']}))
    });
  }
}
