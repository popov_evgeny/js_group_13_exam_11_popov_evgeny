import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../../models/product.moedl';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/type';
import { fetchProductsRequest } from '../../store/products.actions';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.sass']
})
export class ProductsComponent implements OnInit {
  products: Observable<Product[]>
  loading: Observable<boolean>
  error: Observable<null | string>
  apiUrl = environment.apiUrl;

  constructor(private store: Store<AppState>) {
    this.products = store.select(state => state.products.products);
    this.loading = store.select(state => state.products.fetchLoading);
    this.error = store.select(state => state.products.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchProductsRequest());
  }

}
