import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/type';
import { fetchProductCategoryRequest, fetchProductRequest, removeProductRequest } from '../../store/products.actions';
import { Observable, Subscription } from 'rxjs';
import { Product, RemoveProduct } from '../../models/product.moedl';
import { environment } from '../../../environments/environment';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass']
})
export class ProductComponent implements OnInit, OnDestroy{
  product: Observable<Product | null>;
  productSubscription!: Subscription;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  productInformation!: Product;
  apiUrl = environment.apiUrl
  user: Observable<null | User>;
  userSubscription!: Subscription;
  userId!: string;
  userToken!: string;
  idProduct!: string;

  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>
  ) {
    this.product = store.select( state => state.products.product);
    this.loading = store.select( state => state.products.fetchLoading);
    this.error = store.select( state => state.products.fetchError);
    this.user = store.select(state => state.user?.user);
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.store.dispatch(fetchProductRequest({id: params['id']}))
    });
    this.productSubscription = this.product.subscribe( product => {
      this.productInformation = <Product>product;
    })
    this.userSubscription = this.user.subscribe( user => {
      if (user) {
        this.userToken = user?.token;
        this.userId = user?._id;
      }
    })
    this.route.params.subscribe( params => {
      this.idProduct = params['id'];
    })
  }

  ngOnDestroy() {
    this.productSubscription.unsubscribe();
  }

  onRemove() {
    let removeProduct: RemoveProduct = {
      id: this.idProduct,
      token: this.userToken
    };

    this.store.dispatch(removeProductRequest({removeProduct}));
  }
}
