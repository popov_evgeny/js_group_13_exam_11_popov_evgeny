import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { ApiUser, RegisterError } from '../../models/user.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/type';
import { registerRequest } from '../../store/users.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements AfterViewInit, OnDestroy {
  @ViewChild('form') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<null | RegisterError>;
  errSubscription!: Subscription;


  constructor(private store: Store<AppState>) {
    this.loading = store.select( state => state.user.registerLoading);
    this.error = store.select( state => state.user.registerError);
  }

  ngAfterViewInit(): void {
    this.errSubscription = this.error.subscribe(error => {
      if (error) {
        const message = error.errors.email.message;
        this.form.form.get('email')?.setErrors({serverError: message});
      } else {
        this.form.form.get('email')?.setErrors({});
      }
    });
  }

  onSubmit() {
    const userData: ApiUser = {
      email: this.form.value.email,
      password: this.form.value.password,
      name: this.form.value.name,
      phone: this.form.value.phone
    }
    this.store.dispatch(registerRequest({userData}));
  }

  ngOnDestroy() {
    this.errSubscription.unsubscribe();
  }
}
