import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ProductsComponent } from './pages/products/products.component';
import { CreateProductsComponent } from './pages/create-products/create-products.component';
import { ProductComponent } from './pages/product/product.component';
import { ProductCategoryComponent } from './pages/product-category/product-category.component';

const routes: Routes = [
  { path: '', component: ProductsComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'products/new', component: CreateProductsComponent},
  { path: ':id/information', component: ProductComponent},
  { path: ':id/category', component: ProductCategoryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
