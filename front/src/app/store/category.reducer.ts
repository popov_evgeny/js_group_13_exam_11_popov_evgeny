import { CategoryState } from './type';
import { createReducer, on } from '@ngrx/store';
import { fetchCategoryFailure, fetchCategoryRequest, fetchCategorySuccess } from './category.actions';

const initialState: CategoryState = {
  category: [],
  error: null,
  loading: false,
}

export const categoryReducer = createReducer(
  initialState,
  on(fetchCategoryRequest, state => ({...state, loading: true, error: null})),
  on(fetchCategorySuccess, (state, {category}) => ({...state, loading: false, category})),
  on(fetchCategoryFailure, (state, {error}) => ({...state, loading: false, error: error})),
)
