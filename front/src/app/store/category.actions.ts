import { createAction, props } from '@ngrx/store';
import { CategoryModel } from '../models/category.model';

export const fetchCategoryRequest = createAction('[Category] Fetch Request');
export const fetchCategorySuccess = createAction('[Category] Fetch Success', props<{category: CategoryModel[]}>());
export const fetchCategoryFailure = createAction('[Category] Fetch Failure', props<{error: null | string}>());
