import { createReducer, on } from '@ngrx/store';
import {
  createProductFailure,
  createProductRequest,
  createProductSuccess, fetchProductCategoryFailure,
  fetchProductCategoryRequest,
  fetchProductCategorySuccess,
  fetchProductFailure,
  fetchProductRequest,
  fetchProductsFailure,
  fetchProductsRequest,
  fetchProductsSuccess,
  fetchProductSuccess, removeProductFailure, removeProductRequest, removeProductSuccess
} from './products.actions';
import { ProductsState } from './type';

const initialState: ProductsState = {
  products: [],
  product: null,
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
};

export const productsReducer = createReducer(
  initialState,
  on(fetchProductsRequest, state => ({...state, fetchLoading: true})),
  on(fetchProductsSuccess, (state, {products}) => ({...state, fetchLoading: false, products})),
  on(fetchProductsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchProductCategoryRequest, state => ({...state, fetchLoading: true})),
  on(fetchProductCategorySuccess, (state, {products}) => ({...state, fetchLoading: false, products})),
  on(fetchProductCategoryFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchProductRequest, state => ({...state, fetchLoading: true})),
  on(fetchProductSuccess, (state, {product}) => ({...state, fetchLoading: false, product})),
  on(fetchProductFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createProductRequest, state => ({...state, createLoading: true})),
  on(createProductSuccess, state => ({...state, createLoading: false})),
  on(createProductFailure, (state, {error}) => ({...state, createLoading: false, createError: error,})),

  on(removeProductRequest, state => ({...state, createLoading: true})),
  on(removeProductSuccess, state => ({...state, createLoading: false})),
  on(removeProductFailure, (state, {error}) => ({...state, createLoading: false, createError: error,}))
);
