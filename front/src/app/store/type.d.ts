import { LoginError, RegisterError, User } from '../models/user.model';
import { Product } from '../models/product.moedl';
import { CategoryModel } from '../models/category.model';

export type ProductsState = {
  products: Product[],
  product: null | Product,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
};

export type UserState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError
};

export type CategoryState = {
  category: CategoryModel[],
  error: null | string,
  loading: boolean
}

export type AppState = {
  products: ProductsState,
  user: UserState,
  category: CategoryState
}
