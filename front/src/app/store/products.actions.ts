import { createAction, props } from '@ngrx/store';
import { Product, ProductData, RemoveProduct } from '../models/product.moedl';

export const fetchProductsRequest = createAction('[Products] Fetch Request');
export const fetchProductsSuccess = createAction('[Products] Fetch Success', props<{products: Product[]}>());
export const fetchProductsFailure = createAction('[Products] Fetch Failure', props<{error: string}>());

export const fetchProductCategoryRequest = createAction('[ProductCategory] Fetch Request', props<{id: string}>());
export const fetchProductCategorySuccess = createAction('[ProductCategory] Fetch Success', props<{products: Product[]}>());
export const fetchProductCategoryFailure = createAction('[ProductCategory] Fetch Failure', props<{error: string}>());

export const createProductRequest = createAction('[Product] Create Request', props<{productData: ProductData}>());
export const createProductSuccess = createAction('[Product] Create Success');
export const createProductFailure = createAction('[Product] Create Failure', props<{error: string}>());

export const fetchProductRequest = createAction('[Product] Fetch Request', props<{id: string}>());
export const fetchProductSuccess = createAction('[Product] Fetch Success', props<{product: Product}>());
export const fetchProductFailure = createAction('[Product] Fetch Failure', props<{error: string}>());

export const removeProductRequest = createAction('[Product] Remove Request', props<{removeProduct: RemoveProduct}>());
export const removeProductSuccess = createAction('[Product] Remove Success');
export const removeProductFailure = createAction('[Product] Remove Failure', props<{error: string}>());
