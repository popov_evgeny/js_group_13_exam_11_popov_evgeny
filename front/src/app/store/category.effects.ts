import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { fetchCategoryFailure, fetchCategoryRequest, fetchCategorySuccess } from './category.actions';
import { Router } from '@angular/router';
import { HelpersService } from '../services/helper.service';
import { CategoryService } from '../services/category.service';

@Injectable()
export class CategoriesEffects {

  constructor(
    private actions: Actions,
    private categoryService: CategoryService,
    private helpers: HelpersService,
    private router: Router
  ) {}

  fetchCategories = createEffect(() => this.actions.pipe(
    ofType(fetchCategoryRequest),
    mergeMap(() => this.categoryService.getCategories().pipe(
      map(category => fetchCategorySuccess({category})),
      tap(() => {
        this.helpers.openSnackbar('Register successful');
        void this.router.navigate(['/']);
      }),
      catchError(() => of(fetchCategoryFailure({
        error: 'Error'
      })))
    ))
  ));


}
