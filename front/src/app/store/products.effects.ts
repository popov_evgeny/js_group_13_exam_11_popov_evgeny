import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ProductsService } from '../services/products.service';
import {
  createProductFailure,
  createProductRequest,
  createProductSuccess, fetchProductCategoryFailure,
  fetchProductCategoryRequest,
  fetchProductCategorySuccess,
  fetchProductFailure,
  fetchProductRequest,
  fetchProductsFailure,
  fetchProductsRequest,
  fetchProductsSuccess,
  fetchProductSuccess, removeProductFailure, removeProductRequest, removeProductSuccess
} from './products.actions';
import { mergeMap, map, catchError, of, tap } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class ProductsEffects {

  constructor(
    private actions: Actions,
    private productsService: ProductsService,
    private router: Router
  ) {}

  fetchProducts = createEffect(() => this.actions.pipe(
    ofType(fetchProductsRequest),
    mergeMap(() => this.productsService.getProducts().pipe(
      map(products => fetchProductsSuccess({products})),
      catchError(() => of(fetchProductsFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  fetchProduct = createEffect(() => this.actions.pipe(
    ofType(fetchProductRequest),
    mergeMap(id => this.productsService.getProduct(id.id).pipe(
      map( product => fetchProductSuccess( {product})),
      catchError( () => of(fetchProductFailure({error: 'Error!'})))
    ))
  ));

  fetchProductsCategory = createEffect(() => this.actions.pipe(
    ofType(fetchProductCategoryRequest),
    mergeMap(id => this.productsService.getProductsCategory(id.id).pipe(
      map( products => fetchProductCategorySuccess( {products})),
      catchError( () => of(fetchProductCategoryFailure({error: 'Error!'})))
    ))
  ));

  createProduct = createEffect(() => this.actions.pipe(
    ofType(createProductRequest),
    mergeMap(({productData}) => this.productsService.createProduct(productData).pipe(
      map(() => createProductSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createProductFailure({error: 'Wrong data'})))
    ))
  ));

  removeProduct = createEffect(() => this.actions.pipe(
    ofType(removeProductRequest),
    mergeMap(({removeProduct}) => this.productsService.removeProduct(removeProduct).pipe(
      map(() => removeProductSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(removeProductFailure({error: 'Wrong data'})))
    ))
  ));

}
