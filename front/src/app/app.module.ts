import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './pages/login/login.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RegisterComponent } from './pages/register/register.component';
import { FormCardComponent } from './ui/form-card/form-card.component';
import { MatCardModule } from '@angular/material/card';
import { FlexModule } from '@angular/flex-layout';
import { ProductsComponent } from './pages/products/products.component';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ProductsEffects } from './store/products.effects';
import { productsReducer } from './store/products.reducer';
import { usersReducer } from './store/users.reducer';
import { UsersEffects } from './store/users.effects';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { localStorageSync } from 'ngrx-store-localstorage';
import { MatMenuModule } from '@angular/material/menu';
import { CreateProductsComponent } from './pages/create-products/create-products.component';
import { FileInputComponent } from './ui/file-input/file-input.component';
import { MatSelectModule } from '@angular/material/select';
import { categoryReducer } from './store/category.reducer';
import { CategoriesEffects } from './store/category.effects';
import { ProductComponent } from './pages/product/product.component';
import { ProductCategoryComponent } from './pages/product-category/product-category.component';
import { ValidatePasswordDirective } from './services/validate-password.directive';

const localStorageSyncReducer = (reducer: ActionReducer<any>) => {
  return localStorageSync({
    keys: [{user: ['user']}],
    rehydrate: true
  })(reducer);
}

const metaReducers: MetaReducer[] = [localStorageSyncReducer];

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LoginComponent,
    RegisterComponent,
    FormCardComponent,
    ProductsComponent,
    CreateProductsComponent,
    FileInputComponent,
    ProductComponent,
    ProductCategoryComponent,
    ValidatePasswordDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatSnackBarModule,
    FlexModule,
    StoreModule.forRoot({
      products: productsReducer,
      user: usersReducer,
      category: categoryReducer
    }, {metaReducers}),
    EffectsModule.forRoot([ProductsEffects, UsersEffects, CategoriesEffects]),
    MatProgressSpinnerModule,
    MatMenuModule,
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
