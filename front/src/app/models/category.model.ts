export class CategoryModel {
  constructor(
    public _id: string,
    public title: string
  ) {
  }
}

export interface ApiCategory {
  _id: string,
  title: string
}
