export class Product {
  constructor(
    public id: string,
    public title: string,
    public category: {
      _id: string,
      title: string
    },
    public price: number,
    public description: string,
    public image: string,
    public user: {
      _id: string,
      name: string
    }
  ) {}
}

export interface ProductData {
  [key: string]: any;
  title: string;
  price: number;
  description: string;
  image: File | null;
  category: string;
  token: string;
}

export interface ApiProductData {
  _id: string,
  title: string,
  category: {
    _id: string,
    title: string
  },
  price: number,
  description: string,
  image: string,
  user: {
    _id: string,
    name: string
  }
}

export interface RemoveProduct {
  id: string,
  token: string
}
