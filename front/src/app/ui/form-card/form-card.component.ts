import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-form-card',
  templateUrl: './form-card.component.html',
  styleUrls: ['./form-card.component.sass']
})
export class FormCardComponent {
  @Input() title!: string;
}
