import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { User } from '../../models/user.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/type';
import { logoutRequest } from '../../store/users.actions';
import { CategoryModel } from '../../models/category.model';
import { fetchCategoryRequest } from '../../store/category.actions';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit{

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  user: Observable<null | User>;
  categories: Observable<CategoryModel[]>;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private store: Store<AppState>
  ) {
    this.user = store.select(state => state.user.user);
    this.categories = store.select(state => state.category.category);
  }

  ngOnInit() {
    this.store.dispatch(fetchCategoryRequest());
  }

  logout() {
    this.store.dispatch(logoutRequest());
  }


}
