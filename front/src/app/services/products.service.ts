import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiProductData, Product, ProductData, RemoveProduct } from '../models/product.moedl';
import { map } from 'rxjs/operators';
import { environment as env } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) {
  }

  getProducts() {
    return this.http.get<ApiProductData[]>(env.apiUrl + '/product').pipe(
      map(response => {
        return response.map(productData => {
          return new Product(
            productData._id,
            productData.title,
            productData.category,
            productData.price,
            productData.description,
            productData.image,
            productData.user
          );
        });
      })
    );
  }

  getProductsCategory(id: string) {
    return this.http.get<ApiProductData[]>(env.apiUrl + '/product?category=' + id).pipe(
      map(response => {
        return response.map(productData => {
          return new Product(
            productData._id,
            productData.title,
            productData.category,
            productData.price,
            productData.description,
            productData.image,
            productData.user
          );
        });
      })
    );
  }

  getProduct(id: string) {
    return this.http.get<Product>(env.apiUrl + '/product/' + id).pipe(
      map(response => {
        return response;
      })
    )
  }

  createProduct(productData: ProductData) {
    const formData = new FormData();

    Object.keys(productData).forEach(key => {
      if (productData[key] !== null) {
        formData.append(key, productData[key]);
      }
    });

    return this.http.post(env.apiUrl + '/product', formData, {
      headers: new HttpHeaders({'Authorization': productData.token})
    });
  }

  removeProduct(removeProduct: RemoveProduct) {
    return this.http.delete(env.apiUrl + '/product/' + removeProduct.id, {
      headers: new HttpHeaders({'Authorization': removeProduct?.token})
    })
  }
}
