import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { ApiCategory, CategoryModel } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get<ApiCategory[]>(env.apiUrl + '/categories').pipe(
      map(response => {
        return response.map(categoryData => {
          return new CategoryModel(
            categoryData._id,
            categoryData.title,
          );
        });
      })
    );
  }
}
